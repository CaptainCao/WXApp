#WXApp
- [wechat-demo](http://git.oschina.net/codebean/wechat-demo)
微信应用号（小程序）demo，以及开发安装文档

- [wxapp-Breakfast](http://git.oschina.net/hayeah/wxapp-Breakfast)
小林早厨微信小程序

- [finance](http://git.oschina.net/dotton/finance)
微信小程序（应用号）实战课程之记账软件开发,其[服务端篇](https://git.oschina.net/dotton/finance-web)

- [SmallAPP](http://git.oschina.net/qieangel2013/SmallAPP)
添加微信小程序

- [weapp-webdemo](http://git.oschina.net/phodal/weapp-webdemo)
「微信小程序」 Web Demo

- [Wechat App Demo](http://git.oschina.net/maclin/Wechat-App-Demo)
微信应用，小程序Demo

- [wxapp-cnode](http://git.oschina.net/hayeah/wxapp-cnode)
微信小程序直播 - wxapp-cnode

- [wmusic](http://git.oschina.net/laohuangshu/wmusic)
简易情绪播放器 微信小程序版

- [weixin](http://git.oschina.net/zcoco/weixin)
使用微信小程序、微信小应用仿造【微信】APP

- [WXDropDownMenu](http://git.oschina.net/dotton/WXDropDownMenu)
小程序下拉菜单，可用于筛选

- [news](https://git.oschina.net/dotton/news)
灵犀新闻客户端-基于微信小程序（应用号）开发

- [qqmusic](https://git.oschina.net/caosiyuan/qqmusic/tree/master)
最近微信推出了微信小程序，于是我这边再自己的研究下面基于微信小程序开发的qq音乐

- [weixincalculator](https://git.oschina.net/edik/weixincalculator)
微信小程序-计算器demo

- [WebShakingPiggyBank](https://git.oschina.net/garen_git/WebWechat)
微信小程序版玩抢红包

- [Wechat applet](http://git.oschina.net/uctoo/Wechat-applet)
各种微信小程序案例

>  **其他汇总** 
 **1** . [wechat-weapp-resource](http://git.oschina.net/orangesoft/wechat-weapp-resource)
微信应用号（小程序）资源汇